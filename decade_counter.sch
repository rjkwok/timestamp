EESchema Schematic File Version 4
LIBS:unix_watch-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 11
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L unix_watch-rescue:GND-power-unix_watch-rescue #PWR?
U 1 1 5C8D6E60
P 4350 3950
AR Path="/5C8D6E60" Ref="#PWR?"  Part="1" 
AR Path="/5C8D6ACF/5C8D6E60" Ref="#PWR017"  Part="1" 
AR Path="/5C8FA96D/5C8D6E60" Ref="#PWR024"  Part="1" 
AR Path="/5C8FA9C1/5C8D6E60" Ref="#PWR031"  Part="1" 
AR Path="/5C8FAA15/5C8D6E60" Ref="#PWR038"  Part="1" 
AR Path="/5C8FAA69/5C8D6E60" Ref="#PWR045"  Part="1" 
AR Path="/5C8FAABD/5C8D6E60" Ref="#PWR052"  Part="1" 
AR Path="/5C8FAB11/5C8D6E60" Ref="#PWR059"  Part="1" 
AR Path="/5C8FAB75/5C8D6E60" Ref="#PWR066"  Part="1" 
AR Path="/5C8FABC9/5C8D6E60" Ref="#PWR073"  Part="1" 
AR Path="/5C8FAC31/5C8D6E60" Ref="#PWR080"  Part="1" 
F 0 "#PWR017" H 4350 3700 50  0001 C CNN
F 1 "GND" H 4355 3777 50  0000 C CNN
F 2 "" H 4350 3950 50  0001 C CNN
F 3 "" H 4350 3950 50  0001 C CNN
	1    4350 3950
	1    0    0    -1  
$EndComp
Wire Wire Line
	3550 3650 3600 3650
$Comp
L unix_watch:CD4026BNSR U?
U 1 1 5C8D6E7C
P 4350 3250
AR Path="/5C8D6E7C" Ref="U?"  Part="1" 
AR Path="/5C8D6ACF/5C8D6E7C" Ref="U5"  Part="1" 
AR Path="/5C8FA96D/5C8D6E7C" Ref="U7"  Part="1" 
AR Path="/5C8FA9C1/5C8D6E7C" Ref="U9"  Part="1" 
AR Path="/5C8FAA15/5C8D6E7C" Ref="U11"  Part="1" 
AR Path="/5C8FAA69/5C8D6E7C" Ref="U13"  Part="1" 
AR Path="/5C8FAABD/5C8D6E7C" Ref="U15"  Part="1" 
AR Path="/5C8FAB11/5C8D6E7C" Ref="U17"  Part="1" 
AR Path="/5C8FAB75/5C8D6E7C" Ref="U19"  Part="1" 
AR Path="/5C8FABC9/5C8D6E7C" Ref="U21"  Part="1" 
AR Path="/5C8FAC31/5C8D6E7C" Ref="U23"  Part="1" 
F 0 "U5" H 6650 3727 60  0000 C CNN
F 1 "CD4026BNSR" H 6650 3621 60  0000 C CNN
F 2 "unix_watch:CD4026BNSR" H 6650 3490 60  0001 C CNN
F 3 "" H 6650 3650 60  0000 C CNN
F 4 "296-28693-1-ND" H 6650 3523 50  0000 C CNN "Digikey Part #"
	1    4350 3250
	1    0    0    -1  
$EndComp
Text HLabel 1600 3300 0    50   BiDi ~ 0
Clock
Text HLabel 3550 3650 0    50   BiDi ~ 0
Carry
Text GLabel 9200 2600 2    50   Input ~ 0
3V3
Text GLabel 4350 3450 0    50   Input ~ 0
3V3
$Comp
L unix_watch-rescue:GND-power-unix_watch-rescue #PWR?
U 1 1 5C8FFEED
P 3900 3350
AR Path="/5C8FFEED" Ref="#PWR?"  Part="1" 
AR Path="/5C8D6ACF/5C8FFEED" Ref="#PWR015"  Part="1" 
AR Path="/5C8FA96D/5C8FFEED" Ref="#PWR022"  Part="1" 
AR Path="/5C8FA9C1/5C8FFEED" Ref="#PWR029"  Part="1" 
AR Path="/5C8FAA15/5C8FFEED" Ref="#PWR036"  Part="1" 
AR Path="/5C8FAA69/5C8FFEED" Ref="#PWR043"  Part="1" 
AR Path="/5C8FAABD/5C8FFEED" Ref="#PWR050"  Part="1" 
AR Path="/5C8FAB11/5C8FFEED" Ref="#PWR057"  Part="1" 
AR Path="/5C8FAB75/5C8FFEED" Ref="#PWR064"  Part="1" 
AR Path="/5C8FABC9/5C8FFEED" Ref="#PWR071"  Part="1" 
AR Path="/5C8FAC31/5C8FFEED" Ref="#PWR078"  Part="1" 
F 0 "#PWR015" H 3900 3100 50  0001 C CNN
F 1 "GND" H 3905 3177 50  0000 C CNN
F 2 "" H 3900 3350 50  0001 C CNN
F 3 "" H 3900 3350 50  0001 C CNN
	1    3900 3350
	1    0    0    -1  
$EndComp
Wire Wire Line
	3900 3250 4350 3250
Wire Wire Line
	4350 3350 3900 3350
Text Notes 4550 2300 0    50   ~ 0
Single cathode resistor makes the clock LEDs dimmer with each additional segment that's lit but that might be ok for a janky clock
Text Notes 4050 6400 0    50   ~ 0
Tested with 54 ohms and 3.3V on breadboard, almost no change between 1 seg brightness and all segs brightness!\nConfirmed pinout numbering too.\nStill need to print out and check dimensions match up.
$Comp
L Device:C C?
U 1 1 5CFE4D1D
P 9150 2850
AR Path="/5CFE4D1D" Ref="C?"  Part="1" 
AR Path="/5C8D6ACF/5CFE4D1D" Ref="C7"  Part="1" 
AR Path="/5C8FA96D/5CFE4D1D" Ref="C9"  Part="1" 
AR Path="/5C8FA9C1/5CFE4D1D" Ref="C11"  Part="1" 
AR Path="/5C8FAA15/5CFE4D1D" Ref="C13"  Part="1" 
AR Path="/5C8FAA69/5CFE4D1D" Ref="C15"  Part="1" 
AR Path="/5C8FAABD/5CFE4D1D" Ref="C17"  Part="1" 
AR Path="/5C8FAB11/5CFE4D1D" Ref="C19"  Part="1" 
AR Path="/5C8FAB75/5CFE4D1D" Ref="C21"  Part="1" 
AR Path="/5C8FABC9/5CFE4D1D" Ref="C23"  Part="1" 
AR Path="/5C8FAC31/5CFE4D1D" Ref="C25"  Part="1" 
F 0 "C7" H 9265 2896 50  0000 L CNN
F 1 "0.1uF" H 9265 2805 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603" H 9188 2700 50  0001 C CNN
F 3 "~" H 9150 2850 50  0001 C CNN
	1    9150 2850
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5CFE4D24
P 9150 3000
AR Path="/5CFE4D24" Ref="#PWR?"  Part="1" 
AR Path="/5C8D6ACF/5CFE4D24" Ref="#PWR013"  Part="1" 
AR Path="/5C8FA96D/5CFE4D24" Ref="#PWR020"  Part="1" 
AR Path="/5C8FA9C1/5CFE4D24" Ref="#PWR027"  Part="1" 
AR Path="/5C8FAA15/5CFE4D24" Ref="#PWR034"  Part="1" 
AR Path="/5C8FAA69/5CFE4D24" Ref="#PWR041"  Part="1" 
AR Path="/5C8FAABD/5CFE4D24" Ref="#PWR048"  Part="1" 
AR Path="/5C8FAB11/5CFE4D24" Ref="#PWR055"  Part="1" 
AR Path="/5C8FAB75/5CFE4D24" Ref="#PWR062"  Part="1" 
AR Path="/5C8FABC9/5CFE4D24" Ref="#PWR069"  Part="1" 
AR Path="/5C8FAC31/5CFE4D24" Ref="#PWR076"  Part="1" 
F 0 "#PWR013" H 9150 2750 50  0001 C CNN
F 1 "GND" H 9155 2827 50  0000 C CNN
F 2 "" H 9150 3000 50  0001 C CNN
F 3 "" H 9150 3000 50  0001 C CNN
	1    9150 3000
	1    0    0    -1  
$EndComp
Wire Wire Line
	9150 2700 9150 2600
Wire Wire Line
	9150 2600 9200 2600
Wire Wire Line
	8950 2600 9150 2600
Wire Wire Line
	8950 2600 8950 3250
Connection ~ 9150 2600
$Comp
L power:GND #PWR?
U 1 1 5CFE693C
P 10000 3450
AR Path="/5CFE693C" Ref="#PWR?"  Part="1" 
AR Path="/5C8D6ACF/5CFE693C" Ref="#PWR016"  Part="1" 
AR Path="/5C8FA96D/5CFE693C" Ref="#PWR023"  Part="1" 
AR Path="/5C8FA9C1/5CFE693C" Ref="#PWR030"  Part="1" 
AR Path="/5C8FAA15/5CFE693C" Ref="#PWR037"  Part="1" 
AR Path="/5C8FAA69/5CFE693C" Ref="#PWR044"  Part="1" 
AR Path="/5C8FAABD/5CFE693C" Ref="#PWR051"  Part="1" 
AR Path="/5C8FAB11/5CFE693C" Ref="#PWR058"  Part="1" 
AR Path="/5C8FAB75/5CFE693C" Ref="#PWR065"  Part="1" 
AR Path="/5C8FABC9/5CFE693C" Ref="#PWR072"  Part="1" 
AR Path="/5C8FAC31/5CFE693C" Ref="#PWR079"  Part="1" 
F 0 "#PWR016" H 10000 3200 50  0001 C CNN
F 1 "GND" H 10005 3277 50  0000 C CNN
F 2 "" H 10000 3450 50  0001 C CNN
F 3 "" H 10000 3450 50  0001 C CNN
	1    10000 3450
	1    0    0    -1  
$EndComp
Wire Wire Line
	10000 3350 10000 3450
Wire Wire Line
	8950 3350 10000 3350
$Comp
L unix_watch:74AHC1G04GW,125 U4
U 1 1 5CFE85D6
P 2400 2850
AR Path="/5C8D6ACF/5CFE85D6" Ref="U4"  Part="1" 
AR Path="/5C8FAA69/5CFE85D6" Ref="U12"  Part="1" 
AR Path="/5C8FA96D/5CFE85D6" Ref="U6"  Part="1" 
AR Path="/5C8FA9C1/5CFE85D6" Ref="U8"  Part="1" 
AR Path="/5C8FAA15/5CFE85D6" Ref="U10"  Part="1" 
AR Path="/5C8FAABD/5CFE85D6" Ref="U14"  Part="1" 
AR Path="/5C8FAB11/5CFE85D6" Ref="U16"  Part="1" 
AR Path="/5C8FAB75/5CFE85D6" Ref="U18"  Part="1" 
AR Path="/5C8FABC9/5CFE85D6" Ref="U20"  Part="1" 
AR Path="/5C8FAC31/5CFE85D6" Ref="U22"  Part="1" 
F 0 "U4" H 2400 3215 50  0000 C CNN
F 1 "74AHC1G04GW,125" H 2400 3124 50  0000 C CNN
F 2 "TO_SOT_Packages_SMD:SOT-353_SC-70-5" H 2300 3100 50  0001 C CNN
F 3 "" H 2400 2850 50  0001 C CNN
F 4 "1727-3147-1-ND" H 2500 3300 50  0001 C CNN "Digikey Part #"
	1    2400 2850
	1    0    0    -1  
$EndComp
Text GLabel 3050 2100 2    50   Input ~ 0
3V3
$Comp
L Device:C C?
U 1 1 5CFE8FF3
P 3000 2350
AR Path="/5CFE8FF3" Ref="C?"  Part="1" 
AR Path="/5C8D6ACF/5CFE8FF3" Ref="C6"  Part="1" 
AR Path="/5C8FA96D/5CFE8FF3" Ref="C8"  Part="1" 
AR Path="/5C8FA9C1/5CFE8FF3" Ref="C10"  Part="1" 
AR Path="/5C8FAA15/5CFE8FF3" Ref="C12"  Part="1" 
AR Path="/5C8FAA69/5CFE8FF3" Ref="C14"  Part="1" 
AR Path="/5C8FAABD/5CFE8FF3" Ref="C16"  Part="1" 
AR Path="/5C8FAB11/5CFE8FF3" Ref="C18"  Part="1" 
AR Path="/5C8FAB75/5CFE8FF3" Ref="C20"  Part="1" 
AR Path="/5C8FABC9/5CFE8FF3" Ref="C22"  Part="1" 
AR Path="/5C8FAC31/5CFE8FF3" Ref="C24"  Part="1" 
F 0 "C6" H 3115 2396 50  0000 L CNN
F 1 "0.1uF" H 3115 2305 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603" H 3038 2200 50  0001 C CNN
F 3 "~" H 3000 2350 50  0001 C CNN
	1    3000 2350
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5CFE8FFA
P 3000 2500
AR Path="/5CFE8FFA" Ref="#PWR?"  Part="1" 
AR Path="/5C8D6ACF/5CFE8FFA" Ref="#PWR012"  Part="1" 
AR Path="/5C8FA96D/5CFE8FFA" Ref="#PWR019"  Part="1" 
AR Path="/5C8FA9C1/5CFE8FFA" Ref="#PWR026"  Part="1" 
AR Path="/5C8FAA15/5CFE8FFA" Ref="#PWR033"  Part="1" 
AR Path="/5C8FAA69/5CFE8FFA" Ref="#PWR040"  Part="1" 
AR Path="/5C8FAABD/5CFE8FFA" Ref="#PWR047"  Part="1" 
AR Path="/5C8FAB11/5CFE8FFA" Ref="#PWR054"  Part="1" 
AR Path="/5C8FAB75/5CFE8FFA" Ref="#PWR061"  Part="1" 
AR Path="/5C8FABC9/5CFE8FFA" Ref="#PWR068"  Part="1" 
AR Path="/5C8FAC31/5CFE8FFA" Ref="#PWR075"  Part="1" 
F 0 "#PWR012" H 3000 2250 50  0001 C CNN
F 1 "GND" H 3005 2327 50  0000 C CNN
F 2 "" H 3000 2500 50  0001 C CNN
F 3 "" H 3000 2500 50  0001 C CNN
	1    3000 2500
	1    0    0    -1  
$EndComp
Wire Wire Line
	3000 2200 3000 2100
Wire Wire Line
	3000 2100 3050 2100
Wire Wire Line
	2800 2100 3000 2100
Wire Wire Line
	2800 2100 2800 2750
Connection ~ 3000 2100
$Comp
L power:GND #PWR?
U 1 1 5CFE95F3
P 1900 3050
AR Path="/5CFE95F3" Ref="#PWR?"  Part="1" 
AR Path="/5C8D6ACF/5CFE95F3" Ref="#PWR014"  Part="1" 
AR Path="/5C8FA96D/5CFE95F3" Ref="#PWR021"  Part="1" 
AR Path="/5C8FA9C1/5CFE95F3" Ref="#PWR028"  Part="1" 
AR Path="/5C8FAA15/5CFE95F3" Ref="#PWR035"  Part="1" 
AR Path="/5C8FAA69/5CFE95F3" Ref="#PWR042"  Part="1" 
AR Path="/5C8FAABD/5CFE95F3" Ref="#PWR049"  Part="1" 
AR Path="/5C8FAB11/5CFE95F3" Ref="#PWR056"  Part="1" 
AR Path="/5C8FAB75/5CFE95F3" Ref="#PWR063"  Part="1" 
AR Path="/5C8FABC9/5CFE95F3" Ref="#PWR070"  Part="1" 
AR Path="/5C8FAC31/5CFE95F3" Ref="#PWR077"  Part="1" 
F 0 "#PWR014" H 1900 2800 50  0001 C CNN
F 1 "GND" H 1905 2877 50  0000 C CNN
F 2 "" H 1900 3050 50  0001 C CNN
F 3 "" H 1900 3050 50  0001 C CNN
	1    1900 3050
	1    0    0    -1  
$EndComp
Wire Wire Line
	1900 3050 1900 2950
Wire Wire Line
	1900 2950 2000 2950
Wire Wire Line
	1700 2850 1700 3300
Wire Wire Line
	1600 3300 1700 3300
Wire Wire Line
	2000 2850 1700 2850
$Comp
L unix_watch:DS-080-C SW2
U 1 1 5CFF5F1C
P 3550 3250
AR Path="/5C8D6ACF/5CFF5F1C" Ref="SW2"  Part="1" 
AR Path="/5C8FA96D/5CFF5F1C" Ref="SW3"  Part="1" 
AR Path="/5C8FA9C1/5CFF5F1C" Ref="SW4"  Part="1" 
AR Path="/5C8FAA15/5CFF5F1C" Ref="SW5"  Part="1" 
AR Path="/5C8FAA69/5CFF5F1C" Ref="SW6"  Part="1" 
AR Path="/5C8FAABD/5CFF5F1C" Ref="SW7"  Part="1" 
AR Path="/5C8FAB11/5CFF5F1C" Ref="SW8"  Part="1" 
AR Path="/5C8FAB75/5CFF5F1C" Ref="SW9"  Part="1" 
AR Path="/5C8FABC9/5CFF5F1C" Ref="SW10"  Part="1" 
AR Path="/5C8FAC31/5CFF5F1C" Ref="SW11"  Part="1" 
F 0 "SW2" H 3550 3525 50  0000 C CNN
F 1 "DS-080-C" H 3550 3434 50  0000 C CNN
F 2 "unix_watch:DS-080-C" H 3450 3350 50  0001 C CNN
F 3 "" H 3550 3250 50  0001 C CNN
F 4 "CKN10387-ND" H 3650 3550 50  0001 C CNN "Digikey Part #"
	1    3550 3250
	1    0    0    -1  
$EndComp
Wire Wire Line
	3200 3200 3000 3200
Wire Wire Line
	3000 3200 3000 2950
Wire Wire Line
	3000 2950 2800 2950
Wire Wire Line
	3200 3300 1700 3300
Connection ~ 1700 3300
$Comp
L unix_watch:7SEG D1
U 1 1 5CFB6931
P 6850 5400
AR Path="/5C8D6ACF/5CFB6931" Ref="D1"  Part="1" 
AR Path="/5C8FAA15/5CFB6931" Ref="D4"  Part="1" 
AR Path="/5C8FA96D/5CFB6931" Ref="D2"  Part="1" 
AR Path="/5C8FA9C1/5CFB6931" Ref="D3"  Part="1" 
AR Path="/5C8FAA69/5CFB6931" Ref="D5"  Part="1" 
AR Path="/5C8FAABD/5CFB6931" Ref="D6"  Part="1" 
AR Path="/5C8FAB11/5CFB6931" Ref="D7"  Part="1" 
AR Path="/5C8FAB75/5CFB6931" Ref="D8"  Part="1" 
AR Path="/5C8FABC9/5CFB6931" Ref="D9"  Part="1" 
AR Path="/5C8FAC31/5CFB6931" Ref="D10"  Part="1" 
F 0 "D1" H 6850 5865 50  0000 C CNN
F 1 "7SEG" H 6850 5774 50  0000 C CNN
F 2 "unix_watch:7_Segment_14.22_mm" H 6750 5800 50  0001 C CNN
F 3 "" H 6850 5400 50  0001 C CNN
	1    6850 5400
	-1   0    0    -1  
$EndComp
$Comp
L unix_watch-rescue:GND-power-unix_watch-rescue #PWR?
U 1 1 5CFB6C1C
P 6000 5800
AR Path="/5CFB6C1C" Ref="#PWR?"  Part="1" 
AR Path="/5C8D6ACF/5CFB6C1C" Ref="#PWR018"  Part="1" 
AR Path="/5C8FA96D/5CFB6C1C" Ref="#PWR025"  Part="1" 
AR Path="/5C8FA9C1/5CFB6C1C" Ref="#PWR032"  Part="1" 
AR Path="/5C8FAA15/5CFB6C1C" Ref="#PWR039"  Part="1" 
AR Path="/5C8FAA69/5CFB6C1C" Ref="#PWR046"  Part="1" 
AR Path="/5C8FAABD/5CFB6C1C" Ref="#PWR053"  Part="1" 
AR Path="/5C8FAB11/5CFB6C1C" Ref="#PWR060"  Part="1" 
AR Path="/5C8FAB75/5CFB6C1C" Ref="#PWR067"  Part="1" 
AR Path="/5C8FABC9/5CFB6C1C" Ref="#PWR074"  Part="1" 
AR Path="/5C8FAC31/5CFB6C1C" Ref="#PWR081"  Part="1" 
F 0 "#PWR018" H 6000 5550 50  0001 C CNN
F 1 "GND" H 6005 5627 50  0000 C CNN
F 2 "" H 6000 5800 50  0001 C CNN
F 3 "" H 6000 5800 50  0001 C CNN
	1    6000 5800
	1    0    0    -1  
$EndComp
$Comp
L Device:R R2
U 1 1 5CFB6C22
P 6000 5600
AR Path="/5C8D6ACF/5CFB6C22" Ref="R2"  Part="1" 
AR Path="/5C8FA96D/5CFB6C22" Ref="R3"  Part="1" 
AR Path="/5C8FA9C1/5CFB6C22" Ref="R4"  Part="1" 
AR Path="/5C8FAA15/5CFB6C22" Ref="R5"  Part="1" 
AR Path="/5C8FAA69/5CFB6C22" Ref="R6"  Part="1" 
AR Path="/5C8FAABD/5CFB6C22" Ref="R7"  Part="1" 
AR Path="/5C8FAB11/5CFB6C22" Ref="R8"  Part="1" 
AR Path="/5C8FAB75/5CFB6C22" Ref="R9"  Part="1" 
AR Path="/5C8FABC9/5CFB6C22" Ref="R10"  Part="1" 
AR Path="/5C8FAC31/5CFB6C22" Ref="R11"  Part="1" 
F 0 "R2" H 6070 5646 50  0000 L CNN
F 1 "55R" H 6070 5555 50  0000 L CNN
F 2 "Resistors_SMD:R_0603" V 5930 5600 50  0001 C CNN
F 3 "~" H 6000 5600 50  0001 C CNN
	1    6000 5600
	1    0    0    -1  
$EndComp
Wire Wire Line
	6000 5450 6000 5400
Wire Wire Line
	6000 5800 6000 5750
Wire Wire Line
	6000 5400 6350 5400
Wire Wire Line
	7250 5400 7400 5400
Wire Wire Line
	7400 5400 7400 5750
Wire Wire Line
	7400 5750 6350 5750
Wire Wire Line
	6350 5750 6350 5400
Connection ~ 6350 5400
Wire Wire Line
	6350 5400 6450 5400
Wire Wire Line
	7250 5500 9800 5500
Wire Wire Line
	9800 5500 9800 3550
Wire Wire Line
	9800 3550 8950 3550
Wire Wire Line
	9650 3650 9650 5900
Wire Wire Line
	9650 5900 6450 5900
Wire Wire Line
	6450 5900 6450 5600
Wire Wire Line
	6450 5500 6400 5500
Wire Wire Line
	6400 5500 6400 6050
Wire Wire Line
	6400 6050 9550 6050
Wire Wire Line
	9550 6050 9550 3850
Wire Wire Line
	9550 3850 8950 3850
Wire Wire Line
	8950 3650 9650 3650
Wire Wire Line
	8950 3750 9350 3750
Wire Wire Line
	9350 3750 9350 5200
Wire Wire Line
	9350 5200 7250 5200
Wire Wire Line
	8950 3950 9100 3950
Wire Wire Line
	9100 3950 9100 5300
Wire Wire Line
	9100 5300 7250 5300
Wire Wire Line
	6450 5200 4200 5200
Wire Wire Line
	4200 5200 4200 3850
Wire Wire Line
	4200 3850 4350 3850
Wire Wire Line
	4350 3750 4100 3750
Wire Wire Line
	4100 3750 4100 5300
Wire Wire Line
	4100 5300 6450 5300
$Comp
L Device:R R12
U 1 1 5D038070
P 3750 3650
AR Path="/5C8D6ACF/5D038070" Ref="R12"  Part="1" 
AR Path="/5C8FA96D/5D038070" Ref="R13"  Part="1" 
AR Path="/5C8FA9C1/5D038070" Ref="R14"  Part="1" 
AR Path="/5C8FAA15/5D038070" Ref="R15"  Part="1" 
AR Path="/5C8FAA69/5D038070" Ref="R16"  Part="1" 
AR Path="/5C8FAABD/5D038070" Ref="R17"  Part="1" 
AR Path="/5C8FAB11/5D038070" Ref="R18"  Part="1" 
AR Path="/5C8FAB75/5D038070" Ref="R19"  Part="1" 
AR Path="/5C8FABC9/5D038070" Ref="R20"  Part="1" 
AR Path="/5C8FAC31/5D038070" Ref="R21"  Part="1" 
F 0 "R12" V 3957 3650 50  0000 C CNN
F 1 "0R" V 3866 3650 50  0000 C CNN
F 2 "Resistors_SMD:R_0603" V 3680 3650 50  0001 C CNN
F 3 "~" H 3750 3650 50  0001 C CNN
	1    3750 3650
	0    -1   -1   0   
$EndComp
Wire Wire Line
	3900 3650 4350 3650
Text Notes 3100 2900 0    50   ~ 0
do we need debounce here?
$EndSCHEMATC
